import Swal from "sweetalert2";
import moment from 'moment'
import { fetchWithoutToken, fetchWithToken } from "../helpers/fetch";
import { types } from "../types/types";


export const SingIn = ( user ) => ({
    type: types.authSingIn,
    payload: user,
})

export const startSingIn = ( email, password ) => {
    return async ( dispatch ) => {
       const resp = await fetchWithoutToken('auth/singin', {email, password}, 'POST');
    //    console.log(await resp.json())
       const { ok, msg, user, token, errors} = await resp.json();

       if (ok) {
           
           localStorage.setItem('token', token);
        //    localStorage.setItem('timeToken', new Date().getTime());
        // console.log(first)
           localStorage.setItem('timeToken', moment().add(1, 'minutes').toDate());
    
           dispatch(SingIn({
               uid: user.uid,
               name: user.name,
               email: user.email,
               google: user.google,
               img: user.img,
           }))
           Swal.fire('Success', msg, 'success');

        //    dispatch( login( { uid: user.uid, name: user.name }));
       } else{
        //    if(errors){
        // }
        Swal.fire('Error', msg || errors.errors[0].msg, 'error');
        //    Swal.fire('Error', msg, 'error');
       }

    }
}

export const startLoginWithGoogle = ( tokenID ) => {
    return async ( dispatch ) => {
       const resp = await fetchWithoutToken('auth/google', {tokenID}, 'POST');
    //    console.log(await resp.json())
       const { ok, msg, token, errors, uid, name, email, google, img} = await resp.json();

       if (ok) {
           
           localStorage.setItem('token', token);
           localStorage.setItem('timeToken', new Date().getTime());
    
           dispatch(SingIn({
               uid,
               name,
               email,
               google,
               img,
           }))
           Swal.fire('Success', msg, 'success');

        //    dispatch( login( { uid: user.uid, name: user.name }));
       } else{
        //    if(errors){
        // }
        Swal.fire('Error', msg || errors.errors[0].msg, 'error');
        //    Swal.fire('Error', msg, 'error');
       }

    }
}

export const startSingUp = ( name, email, password ) => {

    return async ( dispatch ) => {
        const resp = await fetchWithoutToken('auth/register', {name, email, password, rol: 'User_role'}, 'POST');
        // console.log( await resp.json())
        const { ok, msg, user, token, errors }  = await resp.json();

        if(ok){
            localStorage.setItem('token', token);
            localStorage.setItem('timeToken', new Date().getTime());

            // dipatch al singin
            // Swal
            console.log(user)

            dispatch(SingIn({
                uid: user.uid,
                name: user.name,
                email: user.email,
                google: user.google,
            }))
            Swal.fire('Success', msg, 'success');
        } else {
            Swal.fire('Error', msg || errors.errors[0].msg, 'error');
        }
    }

}

export const startChecking = ( ) => {
    return async( dispatch ) => {

        const resp = await fetchWithToken('auth/renew');
        const { ok, msg, user, uid, name, token } = await resp.json();
        console.log(user)

        if ( ok ) {
            localStorage.setItem('token', token);
            localStorage.setItem('timeToken', new Date().getTime());  

            dispatch(SingIn({
                uid,
                name,
                email: user.email,
                google: user.google,
                img: user.img,
            }))
            // Swal.fire('Success', msg, 'success');
            // dispatch( login( { uid, name: user }));
        } else {
        //    Swal.fire('Error', msg, 'error');
           dispatch( checkingFinish( ) );

        }

    }
}

const checkingFinish = ( ) => ({ type: types.authCheckingFinish });

export const startLogout = ( ) => {
    return ( dispatch ) => {
        localStorage.clear();

        dispatch( logout( ) );

        // para eliminar la data de los otros reducers
        // dispatch(eventLogout());
    }
}

export const logout = ( ) => ({ type: types.authLogout});