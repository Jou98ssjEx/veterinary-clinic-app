import { types } from "../types/types";


export const seeMsg = ( msg = 'Error' ) => ({
    type: types.checkMsgData,
    payload: msg,
})

export const sawMsg = ( ) => ({
    type: types.checkMsg,
})