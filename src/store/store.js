import { applyMiddleware, compose, createStore } from "redux";
import thunk from "redux-thunk";
import { rootReducers } from "../reducers/rootReducers";

// configuracion de las devTools de redux
window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;


// el corazon de redux: el store
export const store = createStore(
    // traer todos los reducers
    rootReducers,
    composeEnhancers(
        // aplicar middleware para las acciones asincronas
        applyMiddleware(thunk)
    )
)