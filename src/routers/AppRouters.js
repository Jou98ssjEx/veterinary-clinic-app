import React, { useEffect } from 'react'
import moment from 'moment'
import { useDispatch, useSelector } from 'react-redux'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Loading } from '../components/ui/Loading'
import { Auth } from '../pages/Auth'
import { VeterinaryAdmin } from '../pages/VeterinaryAdmin'
import { PrivateRoutes } from './PrivateRoutes'
import { PublicRoutes } from './PublicRoutes'
import { startChecking } from '../actions/auth.actions'

export const AppRouters = () => {

  const dispatch = useDispatch();
  const { checking, uid } = useSelector( state => state.auth );

  useEffect(() => {
    dispatch(startChecking());
  }, [dispatch])

  // useEffect(() => {
  //   // renovar el token si existe en localStorage
  // }, [])
  // const tokenExp = moment();
  // // const tokenExp = moment().add(4, 'hours');
  // console.log(tokenExp.toDate())

  // const exp = localStorage.getItem('timeToken');
  // console.log( exp );

  // useEffect(() => {

  //   if( tokenExp === exp ){
  //     // se acabo la authenticcion
  //     console.log('Authenticar')
  //   } else {
  //     // mantiene auth
  //     console.log('Login')
  //   }

    
  // }, [tokenExp, exp])
  

  if( checking ){
    return <Loading />
  }
  
  return (
    <BrowserRouter>
        <Routes>

            {/* <Route path='/auth' element={ <Auth />} /> 
            <Route path='/' element={ <VeterinaryAdmin />} /> */}
            <Route path='/*' element={
                <PrivateRoutes isLoggedIn={ !!uid }>
                  <VeterinaryAdmin />
                </PrivateRoutes>
              }
            />

            <Route path='/auth/*' element={
                <PublicRoutes isLoggedIn={ !!uid }>
                  <Auth />
                </PublicRoutes>
              }
            />

        </Routes>
    </BrowserRouter>
  )
}
