import React from 'react'
import { useSelector } from 'react-redux';

export const MsgError = ( ) => {

  const { checkMsg } = useSelector( state => state.ui );

  return (
    <div className="alert alert-danger text-center mb-2 p-1" role="alert">
        {
          checkMsg
        }
    </div>
  )
}
