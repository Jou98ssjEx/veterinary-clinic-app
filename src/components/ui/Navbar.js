import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startLogout } from '../../actions/auth.actions';
// import { startLogout } from '../../actions/auth';

export const Navbar = () => {
  const { name } = useSelector(state => state.auth); 
  const dispatch =  useDispatch();

  const handleLogout = ( ) => {
    dispatch(startLogout());
  }

  return (
    <div className='navbar navbar-dark bg-dark mb-4'>
        <div className='container-fluid'>
            {/* <span className='ml-3 navbar-brand'>Name </span> */}
            <span className='ml-3 navbar-brand'> { name }</span>

            <button 
              className='mr-3 btn btn-outline-danger'
              onClick={ handleLogout }
            >
                <i className='fas fa-sign-out-alt'></i>
                <span> Exit</span>
            </button>
        </div>
    </div>
  )
}
