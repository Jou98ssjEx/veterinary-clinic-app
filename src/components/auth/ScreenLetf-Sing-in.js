import React from 'react'
import { useDispatch } from 'react-redux';
import { sawMsg } from '../../actions/ui.actions';

export const ScreenLetfSingin = ( { setAuth } ) => {

    const dipatch = useDispatch();
    
    const handleClick = ( ) => {
        setAuth(false);
        dipatch(sawMsg());
    }

  return (
    <div className='col-12 col-md-6'>
        <div className=''>
            <div className="">
                <div className=" text-center me-3">
                    <h3>New here ?</h3>
                    <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis,
                        ex ratione. Aliquid!
                    </p>
                    <button onClick={ handleClick } className="btn btn-light w-50" >
                        Sign up
                    </button>
                </div>
                <img src="/assets/dog2.svg" className="mt-5 auth__image" alt="" />
            </div>
        </div>
    </div>
  )
}
