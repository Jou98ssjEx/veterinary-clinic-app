import React from 'react'
import { useDispatch } from 'react-redux';
import { sawMsg } from '../../actions/ui.actions';

export const ScreenRigthSingup = ({setAuth}) => {

    const dipatch = useDispatch();

    const handleClick = ( ) => {
        setAuth(true);
        dipatch(sawMsg());
      }
  return (
    <div className='col-12 col-md-6'>
        <div className='auth__label_panel'>
            <div className="">
                <div className=" text-center me-3">
                    <h3>New here ?</h3>
                    <p>
                        Lorem ipsum, dolor sit amet consectetur adipisicing elit. Debitis,
                        ex ratione. Aliquid!
                    </p>
                    <button onClick={ handleClick } className="btn btn-light w-50" >
                        Sign IN
                    </button>
                </div>
                <img src="/assets/doctor.svg" className="mt-5 auth__image" alt="" />
            </div>
        </div>
    </div>

  )
}
