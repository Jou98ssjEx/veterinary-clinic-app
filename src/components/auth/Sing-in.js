import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { startSingIn } from '../../actions/auth.actions';

import {useForm} from '../../hooks/useForm'
import { MsgError } from '../ui/MsgError';
import { ScreenLetfSingin } from './ScreenLetf-Sing-in';
import { sawMsg, seeMsg } from '../../actions/ui.actions';
import { GoogleSingIn } from './GoogleSingIn';

export const Singin = ( { setAuth }) => {

    const dispatch = useDispatch();
    const { checkMsg } = useSelector( state => state.ui );

    const initialState = {
        email: 'barry@gmail.com',
        password: '1234567',
    }

    // const [msgEror, setMsgEror] = useState(true);

    const [ formValues, hadleChangeInput, reset ] = useForm( initialState);

    const { email, password } = formValues;

    const handleLogin = ( e ) => {
        e.preventDefault();

        if( isFormValid() ) {
            dispatch(
                startSingIn(email, password),
                // SingIn( formValues)
            )
            console.log(formValues)
        }
    }

    const isFormValid = ( ) => {
        const msg = ``;

        if (email.trim().length === 0){
            console.log('Error');
            dispatch(seeMsg(`Email is required`));
            // setMsgEror(false)
            return false;
        } else if ( password.trim().length < 2 ){
            console.log('Error password')
            dispatch(seeMsg(`Password is required`));

            // setMsgEror(false)
            return false;
        }
        // setMsgEror(true);
        dispatch(sawMsg());
        return true;
    }



 
  return (
    <div className='auth__circle'>
        <div className='row auth__content'>

            <ScreenLetfSingin setAuth={ setAuth } />

            <div className='col-12 col-md-6'>
                <div
                    className='auth__form__container card'
                >
                    <h2 className='text-center mt-3'> Sing In </h2>

                    <div className=''>
                        {
                            ( checkMsg !== null ) && <MsgError />

                        }

                        <form
                            className='container'
                            onSubmit={ handleLogin }
                        >
                            <div className='mb-2 form-group '>
                                <i className='auht_title_icon fas fa-user me-2 '></i>
                                <label className='ml-2 auht_title_icon'> Email: </label>

                                <input
                                    type='text'
                                    // className={`form-control mt-2  ${ (!msgEror) && 'is-invalid'}`}
                                    className={`form-control mt-2 `}
                                    value={ email }
                                    name='email'
                                    onChange={ hadleChangeInput }
                                    autoFocus='on'
                                    // onFocus='on'
                                />
                            </div>

                            <div className='mb-2 form-group'>
                                <i className=' auht_title_icon fas fa-lock me-2 '> </i>
                                <label className='auht_title_icon ml-2 '> Password: </label>
                                <input
                                    type='password'
                                    // className='form-control mt-2'
                                    className={`form-control mt-2 `}
                                    // className={`form-control mt-2  ${ (!msgEror) && 'is-invalid'}`}
                                    value={ password }
                                    name='password'
                                    onChange={ hadleChangeInput }
                                />
                            </div>

                            <button
                                className='mt-2 btn auth__btn w-100'
                            >
                                Login
                            </button>

                        </form>
                    </div>

                    <div className='mt-3 text-center mb-3'>
                        <p>Or Sign in with Google</p>
                        <div className='auth__social_media'>
                           <GoogleSingIn />
                        {/* <a href="#" className="auth__social_icon">
                            <i className="fab fa-google"></i>
                        </a> */}
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
  )
}
