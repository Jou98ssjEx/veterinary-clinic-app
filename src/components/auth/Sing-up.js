import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { startSingUp } from '../../actions/auth.actions';
import { sawMsg, seeMsg } from '../../actions/ui.actions';
import { useForm } from '../../hooks/useForm';
import { MsgError } from '../ui/MsgError';
import { GoogleSingIn } from './GoogleSingIn';
import { ScreenRigthSingup } from './ScreenRigth-Sing-up';

export const Singup = ({setAuth}) => {

    const dispatch = useDispatch();
    const { checkMsg } = useSelector( state => state.ui);
    
    const initialState = {
        name: 'Barry',
        email: 'barry@gmail.com',
        password: '1234567',
        password2: '1234567'
    }

    const [msgEror, setMsgEror] = useState(true);

    const [formValues, handleInputChange, reset] = useForm( initialState );
    const { name, email, password, password2 } = formValues;

    const handleRegister = ( e ) => {
        e.preventDefault();

        if( isFormValid()) {
            console.log(formValues);

            dispatch(startSingUp(name, email, password));
        }

    }

    const isFormValid = ( ) => {

        if( name.trim().length === 0){
            dispatch(seeMsg(`Name is required`));
            setMsgEror(false)

            return false;
        } else if ( email.trim().length === 0 ){
            dispatch(seeMsg(`Email is required`));
            setMsgEror(false)


            return false;
        } else if ( password.trim().length < 2 ){
            dispatch(seeMsg(`Password is required`));
            setMsgEror(false)

            return false;

        } else if ( password.trim() !== password2.trim()){
            dispatch(seeMsg(`The password are diferent`));
            setMsgEror(false)


            return false;
        }

        setMsgEror(true)
        dispatch(sawMsg());

        return true
    }

  return (
    <div className='auth__circle__singup'>
        <div className='row auth__content__singup'>

            <div className='col-12 col-md-6'>
                <div
                    className='auth__form__container_singup card'
                >
                    <h2 className='text-center mt-3'> Sing Up </h2>

                    {
                        (checkMsg !== null) && <MsgError />
                    }

                    <div className=''>
                        <form
                            className='container'
                            onSubmit={ handleRegister }
                        >
                            <div className='mb-2 form-group '>
                                <i className='auht_title_icon fas fa-user me-2 '></i>
                                <label className='ml-2 auht_title_icon'> Name User: </label>
                                
                                <input 
                                    type='text'
                                    className={`form-control mt-2  `}
                                    name='name'
                                    value={name}
                                    onChange={ handleInputChange}
                                    // className='form-control mt-2 '
                                />
                            </div>

                            <div className='mb-2 form-group '>
                                <i className='auht_title_icon fas fa-user me-2 '></i>
                                <label className='ml-2 auht_title_icon'> Email: </label>
                                
                                <input 
                                    type='text'
                                    className={`form-control mt-2  `}
                                    name='email'
                                    value={email}
                                    onChange={ handleInputChange}
                                />
                            </div>

                            <div className='mb-2 form-group'>
                                <i className=' auht_title_icon fas fa-lock me-2 '> </i>
                                <label className='auht_title_icon ml-2 '> Password: </label>
                                <input 
                                    type='password'
                                    className={`form-control mt-2  `}
                                    name='password'
                                    value={password}
                                    onChange={ handleInputChange}
                                />

                            </div>

                            <div className='mb-2 form-group'>
                                <i className=' auht_title_icon fas fa-lock me-2 '> </i>
                                <label className='auht_title_icon ml-2 '> Confirm Password: </label>
                                <input 
                                    type='password'
                                    className={`form-control mt-2  `}
                                    name='password2'
                                    value={password2}
                                    onChange={ handleInputChange}
                                />
                            </div>

                            <button
                                className='mt-2 btn auth__btn w-100'
                            >
                                Login
                            </button>

                        </form>
                    </div>

                    <div className='mt-3 text-center mb-3'>
                        <p>Or Sign in with Google</p>
                        <div className='auth__social_media'>
                            <GoogleSingIn />
                            {/* <a href="#" className="auth__social_icon">
                                <i className="fab fa-google"></i>
                            </a> */}
                        </div>
                    </div>
                </div>
            </div>

           
            <ScreenRigthSingup setAuth={ setAuth }  />
            
        </div>
    </div>
  )
}
