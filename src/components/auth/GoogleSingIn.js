import React from 'react'
import { useDispatch } from 'react-redux'
import { startLoginWithGoogle } from '../../actions/auth.actions'
import GoogleLogin from 'react-google-login'

export const GoogleSingIn = () => {
    // const clientId = process.env.REACT_APP_CLIENT_ID;
    // console.log(process.env)
    // console.log(clientId)
    const dispatch = useDispatch();

    const responseGoogle = (response) => {
        // console.log(response);
        // Token de google
        // console.log(response.tokenId);
        dispatch(startLoginWithGoogle( response.tokenId) )
      }
    
    const custonButtonGoogle = ( renderProps ) => {
    console.log(renderProps)
    return (
        <button
            onClick={renderProps.onClick}
            disabled={renderProps.disabled}
        >
            Google
        </button>
    )
    }
  return (
    <GoogleLogin
        // clientId={ clientId }
        clientId={ process.env.REACT_APP_CLIENT_ID }
        // clientId={'755195553680-k2gh80fpgb5af76q5pudmk4tbb2k811l.apps.googleusercontent.com'}
        // buttonText=''

        icon={true}

        // render={ custonButtonGoogle }
        // render={renderProps => (
        //     <button
        //         onClick={renderProps.onClick}
        //         disabled={renderProps.disabled}
        //     >
        //         This is my custom Google button
        //     </button>
        //   )}

        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
        isSignedIn={false}
        // className={`auth__social_icon`}
        style={ { backgroundColor: 'red'}}
    />
  )
}
