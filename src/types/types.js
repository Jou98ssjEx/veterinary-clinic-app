
export const types = {
    authSingIn: '[ auth ] Sing In',
    authSingUp: '[ auth ] Sing Up',

    // Check Msg
    checkMsg: '[checkMsg] true', 
    checkMsgData: '[checkMsg] Msg false',

    //check auth
    authChecking: '[ auth ] authChecking',
    authCheckingFinish: '[ auth ] authCheckingFinish',

    authLogout: '[ auth ] authLogout',

    // register
    // authSingUp
}