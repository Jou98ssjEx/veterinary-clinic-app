import React from 'react';
import ReactDOM from 'react-dom';
import { VeterinaryClinicApp } from './VeterinaryClinicApp';

import './styles/style.scss'

ReactDOM.render(
    <VeterinaryClinicApp />,
  document.getElementById('root')
);
