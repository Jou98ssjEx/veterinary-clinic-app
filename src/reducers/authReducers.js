import { types } from "../types/types";

const initialState = {
    checking: true,
}

export const authReducers = ( state = initialState, action ) => {
    switch (action.type) {

        case types.authSingIn:
            return{
                ...state,
                checking: false,
                ...action.payload,
                // uid: '443fedgerge4',
                // name: 'Barry',
                // google: false
            }

        case types.authSingUp:
            return{
                ...initialState,
            }

        case  types.authCheckingFinish:
            return {
                ...state,
                checking: false,
            }

        case types.authLogout:
            return{
                // ...state,
                checking: false,
            }
        
    
        default:
            return state;
    }
}