import { types } from "../types/types";

const initialState = {
    isOpenModal: false,
    checkMsg: null, // No hay error
}
export const uiReducer = ( state = initialState , action ) => {

    switch (action.type) {
        case types.checkMsg:
            return{
                ...state,
                checkMsg: null,
            }

        case types.checkMsgData:
            return{
                ...state,
                checkMsg: action.payload,
            }
        // case types.uiOpenModal:
        //    return{
        //        ...state,
        //        isOpenModal: true
        //    }

        // case types.uiCloseModal:
        //     return{
        //         ...state,
        //         isOpenModal: false
        //     }
    
        default:
           return state
    }

}