import { combineReducers } from "redux";
import { authReducers } from "./authReducers";
import { uiReducer } from "./uiReducer";

export const rootReducers = combineReducers({
    ui: uiReducer,
    auth: authReducers,
})