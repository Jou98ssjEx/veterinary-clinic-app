import React, { useState } from 'react'
import { Singin } from '../components/auth/Sing-in'
import { Singup } from '../components/auth/Sing-up';

export const Auth = () => {

    const [auth, setAuth] = useState(true);

  
  return (
      <>
        {/* <Singin setAuth={setAuth} />
        <Singup /> */}
        {
            (auth) ? <Singin setAuth={setAuth} /> : <Singup setAuth={setAuth} auth={auth}/> 
        }
      </>


  )
}
 